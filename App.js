import React, { Component } from 'react';
import { Provider } from 'react-redux';

import { store } from './app/store'; // Import the store
import Home from './app/components/home'; // Import the component file
import AppNavigation from './app/navigation/AppNavigation';
import { getInitData } from './app/actions/index.js';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppNavigation />
      </Provider>
    );
  }
}
