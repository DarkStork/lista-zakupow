import moment from 'moment';
import * as actionTypes from '../constants/actionTypes.js';
import Connections from '../utils/connections/mainConnections.js';
import OfflineControler from '../utils/offline/offlineControler';
import qs from 'qs';

export const conn = new Connections({ core: 'https://mighty-mesa-99349.herokuapp.com' });

export const offlineCtr = new OfflineControler(5000);

export const login = userData => (dispatch, getState) =>
  new Promise((resolve, reject) => {
    conn
      .login(qs.stringify({ login: userData.login, password: userData.password, 'remember-me': true }))
      .then((response) => {
        dispatch({
          type: actionTypes.LOGIN_SUCCESS,
          payload: { name: 'Jan', last_name: 'Kowalski' },
        });
        dispatch(getInitData());
        resolve();
      })
      .catch((err) => {
        reject();
      });
  });

export const logout = () => (dispatch, getState) => {
  dispatch({
    type: actionTypes.LOGOUT_SUCCESS,
  });
};

export const addListProduct = (product, justUpdate = false) => (dispatch, getState) => {
  if (!justUpdate) {
    dispatch({
      type: actionTypes.ADD_LIST_PRODUCT,
      payload: product,
    });
  }
  conn
    .addListProduct({ ...product, id: undefined })
    .then((response) => {
      dispatch({
        type: actionTypes.ADD_LIST_PRODUCT_SUCCESS,
        payload: response.data,
        id: product.id,
      });
    })
    .catch((err) => {
      console.log(err.response);
      dispatch({
        type: actionTypes.OFFLINE_LIST_ADD,
        list: 'listProductsToAdd',
        payload: product,
      });
    });
};

export const editListProduct = (product, justUpdate = false) => (dispatch, getState) => {
  if (!justUpdate) {
    dispatch({
      type: actionTypes.EDIT_LIST_PRODUCT,
      payload: product,
    });
  }
  conn
    .editListProduct({ ...product })
    .then(() => {})
    .catch((err) => {
      console.log(err.response);
      dispatch({
        type: actionTypes.OFFLINE_LIST_ADD_UPDATE,
        list: 'listProductsToUpdate',
        updateList: 'listProductsToAdd',
        payload: { ...product },
      });
    });
};

export const deleteListProduct = (product, justUpdate = false) => (dispatch, getState) => {
  if (!justUpdate) {
    dispatch({
      type: actionTypes.DELETE_LIST_PRODUCT,
      payload: product,
    });
  }
  conn
    .deleteListProduct(product)
    .then(() => {})
    .catch((err) => {
      dispatch({
        type: actionTypes.OFFLINE_LIST_ADD,
        list: 'listProductsToDelete',
        payload: product,
      });
    });
};

export const checkListProduct = product => (dispatch, getState) => {
  dispatch(editListProduct({ ...product, checked: true }));
};

export const uncheckListProduct = product => (dispatch, getState) => {
  dispatch(editListProduct({ ...product, checked: false }));
};

export const buySelectedProducts = listId => (dispatch, getState) => {
  const { lists } = getState();
  const list = lists.find(l => l.id === listId);
  if (list) {
    list.products.filter(p => p.checked).forEach((product) => {
      dispatch(addAvaProduct({ ...product, groupId: list.groupId }));
      dispatch(deleteListProduct(product));
    });
  }
};

export const getInitData = () => (dispatch, getState) =>
  new Promise((resolve, reject) => {
    conn
      .getGroups()
      .then((response) => {
        const groups = response.data.data;

        dispatch({
          type: actionTypes.GET_GROUPS_SUCCESS,
          payload: groups,
        });
        let promises = groups.map(group => conn.getGroupLists(group.id));

        Promise.all(promises)
          .then((results) => {
            let lists = results.reduce((arr, result) => arr.concat(result.data.data), []);
            promises = lists.map(list => conn.getListContent(list.id));

            Promise.all(promises)
              .then((results) => {
                const products = results.map(result => result.data.data);
                lists = lists.map((list, i) => ({ ...list, products: products[i] || [] }));
                dispatch({
                  type: actionTypes.GET_LISTS_SUCCESS,
                  payload: lists,
                });
                resolve();
              })
              .catch((err) => {
                // console.error(err.response);
              });
          })
          .catch((err) => {
            // console.error(err.response);
          });

        // ---- AVA PRODUCTS -------------------------------------------------

        promises = groups.map(group => conn.getGroupAvaProducts(group.id));
        Promise.all(promises)
          .then((results) => {
            const products = results.reduce((arr, result) => arr.concat(result.data.data), []);
            dispatch({ type: actionTypes.GET_PRODUCTS_SUCCESS, payload: products });
          })
          .catch((err) => {
            console.error(err);
          });

        // ----- CATEGORIES ------------------------------------------------------

        promises = groups.map(group => conn.getGroupCategories(group.id));
        Promise.all(promises)
          .then((results) => {
            const categories = results.reduce((arr, result) => arr.concat(result.data.data), []);
            dispatch({ type: actionTypes.GET_CATEGORIES_SUCCESS, payload: categories });
          })
          .catch((err) => {
            console.error(err);
          });
      })
      .catch((err) => {
        // console.error(err);
      });
  });

// Available products

export const addAvaProduct = (product, justUpdate = false) => (dispatch, getState) => {
  if (!justUpdate) {
    dispatch({
      type: actionTypes.ADD_PRODUCT,
      payload: product,
    });
  }
  conn
    .addAvaProduct({ ...product, id: undefined })
    .then((response) => {
      dispatch({
        type: actionTypes.ADD_PRODUCT_SUCCESS,
        payload: response.data,
        id: product.id,
      });
    })
    .catch((err) => {
      console.log(err.response);
      dispatch({
        type: actionTypes.OFFLINE_LIST_ADD,
        list: 'avaProductsToAdd',
        payload: product,
      });
    });
};

export const editAvaProduct = (product, justUpdate = false) => (dispatch, getState) => {
  if (!justUpdate) {
    dispatch({
      type: actionTypes.EDIT_PRODUCT,
      payload: product,
    });
  }
  conn
    .editAvaProduct({ ...product })
    .then(() => {})
    .catch((err) => {
      console.log(err.response);
      dispatch({
        type: actionTypes.OFFLINE_LIST_ADD_UPDATE,
        list: 'avaProductsToUpdate',
        updateList: 'avaProductsToAdd',
        payload: { ...product },
      });
    });
};

export const deleteAvaProduct = (product, justUpdate = false) => (dispatch, getState) => {
  if (!justUpdate) {
    dispatch({
      type: actionTypes.DELETE_PRODUCT,
      payload: product,
    });
  }
  conn
    .deleteAvaProduct(product)
    .then(() => {})
    .catch((err) => {
      dispatch({
        type: actionTypes.OFFLINE_LIST_ADD,
        list: 'avaProductsToDelete',
        payload: product,
      });
    });
};

export const addAvaProductToList = (product, listId) => (dispatch, getState) => {
  dispatch(addListProduct({ ...product, shoppingListId: listId }));
};

export const addNewList = list => (dispatch, getState) => {
  conn
    .addNewList(list)
    .then((response) => {
      dispatch({
        type: actionTypes.ADD_NEW_LIST,
        payload: { ...response.data, products: [] },
      });
    })
    .catch((err) => {
      console.warn(err.response);
    });
};
