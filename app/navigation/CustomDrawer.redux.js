import { connect } from 'react-redux';
import CustomDrawer from './CustomDrawer';
import { logout } from '../actions/index';

const mapStateToProps = state => ({
  offline: state.offline,
});

const mapDispatchToProps = {
  logout,
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomDrawer);
