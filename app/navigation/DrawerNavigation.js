import { createDrawerNavigator } from 'react-navigation';
import ListsScreen from '../screens/Lists/Lists.redux';
import ProductsScreen from '../screens/Products/Products.redux';
import CustomDrawer from './CustomDrawer.redux';

const DrawerNavigation = createDrawerNavigator(
  {
    Listy: { screen: ListsScreen },
    Produkty: { screen: ProductsScreen },
  },
  {
    contentComponent: CustomDrawer,
    initialRouteName: 'Produkty',
  }
);

export default DrawerNavigation;
