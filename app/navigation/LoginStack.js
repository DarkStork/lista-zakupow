import { createStackNavigator } from 'react-navigation';
import ListsScreen from '../screens/Lists/Lists.js';

// Manifest of possible screens
const LoginStack = createStackNavigator(
  {
    loginScreen: { screen: ListsScreen },
    signupScreen: { screen: ListsScreen },
    forgottenPasswordScreen: {
      screen: ListsScreen,
      navigationOptions: { title: 'Forgot Password' },
    },
  },
  {
    headerMode: 'float',
    navigationOptions: {
      headerStyle: { backgroundColor: '#E73536' },
      title: 'You are not logged in',
      headerTintColor: 'white',
    },
  }
);

export default LoginStack;
