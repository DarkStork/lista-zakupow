import { createStackNavigator } from 'react-navigation';
import LoginScreen from '../screens/Login/Login.redux';
import ItemForm from '../screens/ItemForm/ItemForm.redux';
import ListForm from '../screens/ListForm/ListForm.redux';
import LoadingScreen from '../screens/Loading/Loading.redux';
import DrawerNavigation from './DrawerNavigation';

// Manifest of possible screens
const PrimaryNav = createStackNavigator(
  {
    loadingScreen: { screen: LoadingScreen },
    loginStack: { screen: LoginScreen },
    drawerStack: { screen: DrawerNavigation },
    itemForm: { screen: ItemForm },
    listForm: { screen: ListForm },
  },
  {
    // Default config for all screens
    headerMode: 'none',
    title: 'Main',
    initialRouteName: 'loginStack',
  }
);

export default PrimaryNav;
