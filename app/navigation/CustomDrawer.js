import React from 'react';
import { SafeAreaView } from 'react-navigation';
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  Image,
  StatusBar,
  TouchableNativeFeedback,
} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

const iconCodes = {
  Listy: 'shopping-basket',
  Produkty: 'home',
};
const CustomDrawerContentComponent = (props) => {
  const { activeItemKey, offline } = props;
  const { routes } = props.navigation.state;

  return (
    <View style={{ flex: 1 }}>
      <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
        <Image source={require('../../content/logoImage1.png')} style={styles.backgroundImage} />
        {routes.map((route, i) => {
          const isActive = activeItemKey === route.key;
          return (
            <TouchableNativeFeedback
              onPress={() => {
                props.navigation.navigate(route.key);
              }}
              key={i}
            >
              <View style={[styles.drawerItem, isActive ? styles.drawerItemActive : null]}>
                <View style={styles.itemIcon}>
                  <MaterialIcons
                    name={iconCodes[route.routeName]}
                    size={20}
                    color={isActive ? '#406CF6' : '#757575'}
                  />
                </View>
                <Text style={styles.itemText}>{route.routeName}</Text>
              </View>
            </TouchableNativeFeedback>
          );
        })}

        <TouchableNativeFeedback
          onPress={() => {
            props.logout();
            props.navigation.navigate('loginStack');
          }}
          key="logout"
        >
          <View style={styles.drawerItem}>
            <View style={styles.itemIcon}>
              <MaterialIcons name="exit-to-app" size={20} color="#757575" />
            </View>
            <Text style={styles.itemText}>Wyloguj</Text>
          </View>
        </TouchableNativeFeedback>
      </SafeAreaView>
      {!offline.isOnline ? (
        <View style={styles.noSignalContainer}>
          <MaterialIcons name="sentiment-very-dissatisfied" size={32} color="#DDD" />
          <Text style={styles.noSignalText}>Tryb offline</Text>
        </View>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  drawerItem: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    height: 50,
    paddingHorizontal: 20,
  },
  drawerItemActive: {
    backgroundColor: '#EFEFEF',
  },
  itemText: {
    fontWeight: '800',
  },
  itemIcon: {
    marginRight: 10,
  },
  backgroundImage: {
    backgroundColor: '#ccc',
    resizeMode: 'cover',
    width: '100%',
    height: 100 + StatusBar.currentHeight,
    justifyContent: 'center',
    marginTop: -StatusBar.currentHeight,
  },
  container: {
    flex: 1,
  },
  title: {
    color: '#FBFBFF',
    fontSize: 18,
    fontWeight: '600',
  },
  header: {
    height: 100,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#406CF6',
    alignItems: 'center',
    justifyContent: 'center',
  },
  noSignalContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  noSignalText: {
    fontSize: 18,
    color: '#DDD',
    marginLeft: 10,
  },
});

export default CustomDrawerContentComponent;
