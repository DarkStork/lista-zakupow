import React, { Component } from 'react';
import Modal from 'react-native-modal';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

class SelectModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { showModal, items, onClick } = this.props;
    return (
      <Modal isVisible={showModal} backdropColor="white" backdropOpacity={0.2}>
        <View style={styles.modal}>
          {items.map(item => (
            <TouchableOpacity
              key={item.id}
              onPress={() => {
                onClick(item.id);
              }}
            >
              <View style={styles.modalItem}>
                <Text style={styles.modalItemTitle}>{item.name}</Text>
              </View>
            </TouchableOpacity>
          ))}
        </View>
      </Modal>
    );
  }
}
const styles = StyleSheet.create({
  modal: {
    marginVertical: 100,
    marginHorizontal: 30,
    backgroundColor: 'white',
    borderWidth: 1,
    borderRadius: 3,
    borderColor: '#406CF6',
    paddingVertical: 2,
  },
  modalItem: {
    height: 40,
    borderRadius: 3,
    backgroundColor: '#406CF6',
    marginHorizontal: 4,
    marginVertical: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalItemTitle: {
    color: 'white',
    fontWeight: '800',
  },
});

export default SelectModal;
