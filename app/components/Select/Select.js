import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';

class Select extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <Dropdown {...this.props} inputContainerStyle={[styles.select, this.props.style]} />;
  }
}
const styles = StyleSheet.create({
  select: { borderBottomColor: '#406cf6', borderBottomWidth: 1 },
});

export default Select;
