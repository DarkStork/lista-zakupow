import { connect } from 'react-redux';
import Header from './Header';
import { getInitData } from '../../actions/index';

const mapStateToProps = state => ({
  offline: state.offline,
});

const mapDispatchToProps = {
  getInitData,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
