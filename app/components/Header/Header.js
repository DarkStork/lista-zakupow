import React, { Component } from 'react';
import { StyleSheet, ScrollView, StatusBar, RefreshControl, View } from 'react-native';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }
  refreshView() {
    if (!this.props.offline.isOnline) return;
    this.setState({ refreshing: true });
    this.props
      .getInitData()
      .then(() => {
        this.setState({ refreshing: false });
      })
      .catch(() => {
        this.setState({ refreshing: false });
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          refreshControl={
            this.props.offline.isOnline ? (
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.refreshView.bind(this)}
              />
            ) : null
          }
          contentContainerStyle={styles.header}
        >
          {this.props.children}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 50 + StatusBar.currentHeight,
  },
  header: {
    height: 50 + StatusBar.currentHeight,
    paddingTop: StatusBar.currentHeight,
    paddingHorizontal: 10,
    backgroundColor: '#406CF6',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
});

export default Header;
