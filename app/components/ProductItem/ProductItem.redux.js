import { connect } from 'react-redux';
import ProductItem from './ProductItem';
import { addListProduct } from '../../actions/index';

const mapStateToProps = state => ({
  categories: state.categories,
  groups: state.groups,
});

const mapDispatchToProps = {
  addListProduct,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductItem);
