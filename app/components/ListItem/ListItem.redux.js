import { connect } from 'react-redux';
import ListItem from './ListItem';
import { addListProduct } from '../../actions/index';

const mapStateToProps = state => ({
  categories: state.categories,
});

const mapDispatchToProps = {
  addListProduct,
};

export default connect(mapStateToProps, mapDispatchToProps)(ListItem);
