import React, { Component } from 'react';
import { StyleSheet, FlatList, View, Text, StatusBar, TouchableOpacity } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { Dropdown } from 'react-native-material-dropdown';
import moment from 'moment';
import Swipeout from 'react-native-swipeout';
import { colorKeys } from '../../constants/colors';

const defaultCategoryStyle = {
  colorKey: 'colorkeyDef',
  name: 'Inne',
};

class Lists extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.deleteItem = this.deleteItem.bind(this);
    this.checkItem = this.checkItem.bind(this);
    this.uncheckItem = this.uncheckItem.bind(this);
    this.editItem = this.editItem.bind(this);
  }
  deleteItem() {
    this.props.delete(this.props.item);
  }
  checkItem() {
    this.props.check(this.props.item);
  }
  uncheckItem() {
    this.props.uncheck(this.props.item);
  }
  editItem() {
    const { item } = this.props;
    this.props.navigation.navigate('itemForm', {
      listId: item.shoppingListId,
      edit: true,
      item,
    });
  }
  render() {
    const { item, categories } = this.props;

    const deleteIcon = {
      onPress: this.deleteItem,
      component: (
        <View style={styles.deleteButton} backgroundColor="#F4603F">
          <MaterialIcons name="delete" size={32} color="#FBFBFF" />
        </View>
      ),
      backgroundColor: 'transparent',
    };
    const checkIcon = {
      onPress: this.checkItem,
      component: (
        <View style={styles.deleteButton} backgroundColor="#95B22E">
          <MaterialIcons name="add-shopping-cart" size={32} color="#FBFBFF" />
        </View>
      ),
      backgroundColor: 'transparent',
    };
    const uncheckIcon = {
      onPress: this.uncheckItem,
      component: (
        <View style={styles.deleteButton} backgroundColor="#406CF6">
          <MaterialIcons name="playlist-add" size={32} color="#FBFBFF" />
        </View>
      ),
      backgroundColor: 'transparent',
    };

    const editIcon = {
      onPress: this.editItem,
      component: (
        <View style={styles.deleteButton} backgroundColor="#757575">
          <MaterialIcons name="settings" size={32} color="#FBFBFF" />
        </View>
      ),
      backgroundColor: 'transparent',
    };
    const swipeoutSettings = {
      autoClose: true,
      backgroundColor: '#f8f8fd',
      buttonWidth: 60,
      right: [editIcon, deleteIcon, item.checked ? uncheckIcon : checkIcon],
    };

    const category = categories.find(c => c.id === item.categoryId) || defaultCategoryStyle;
    return (
      <Swipeout {...swipeoutSettings}>
        <View
          style={item.checked ? styles.boughtItem : styles.item}
          borderColor={colorKeys[category.colorKey]}
        >
          <View style={styles.categoryCircle} backgroundColor={colorKeys[category.colorKey]} />
          <View style={styles.titleSection}>
            <Text style={item.checked ? styles.itemTitleCrossed : styles.itemTitle}>
              {item.key}
            </Text>
            <Text style={styles.categoryName}>{category.name}</Text>
          </View>
          <Text style={item.checked ? styles.itemQuantityCrossed : styles.itemQuantity}>
            {item.quantity} {item.quantity ? item.unit : null}
          </Text>
        </View>
      </Swipeout>
    );
  }
}

const styles = StyleSheet.create({
  deleteButton: {
    marginHorizontal: 2,
    marginVertical: 2,
    width: 50,
    height: 50,
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5,
    marginLeft: 0,
    elevation: 2,
  },
  item: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    elevation: 2,
    backgroundColor: '#f8f8fd',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftWidth: 3,
    borderColor: '#7A9BFF',
    flexDirection: 'row',
  },
  boughtItem: {
    height: 50,
    marginHorizontal: 10,
    marginVertical: 5,
    elevation: 1,
    backgroundColor: '#efeff2',
    alignItems: 'center',
    justifyContent: 'center',
    borderLeftWidth: 3,
    borderColor: '#7A9BFF',
    flexDirection: 'row',
  },
  itemTitleCrossed: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '400',
    flex: 0.7,
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  itemTitle: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '400',
    flex: 0.7,
    marginBottom: -15,
  },
  itemQuantity: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '300',
    flex: 0.2,
    flexDirection: 'row',
  },
  itemQuantityCrossed: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '300',
    flex: 0.2,
    flexDirection: 'row',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  categoryCircle: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: '#406CF6',
    marginRight: 10,
    marginLeft: 10,
  },
  titleSection: {
    flex: 0.8,
  },
  categoryName: {
    fontSize: 12,
    opacity: 0.5,
  },
});

export default Lists;
