import React, { Component } from 'react';
import { StyleSheet, TextInput } from 'react-native';

class StyledTextInput extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <TextInput
        underlineColorAndroid="transparent"
        selectionColor="#406cf6"
        placeholderTextColor="#999"
        {...this.props}
        style={[styles.textInput, this.props.style]}
      />
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    backgroundColor: 'transparent',
    height: 32,
    borderColor: '#406cf6',
    borderBottomWidth: 1,
    marginHorizontal: 0,
    fontSize: 16,
  },
});

export default StyledTextInput;
