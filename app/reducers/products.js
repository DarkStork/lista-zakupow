import * as actionTypes from '../constants/actionTypes';

const initState = [];

const products = (state = initState, action) => {
  switch (action.type) {
  case actionTypes.GET_PRODUCTS_SUCCESS:
    return action.payload;
  case actionTypes.ADD_PRODUCT:
    return [...state, action.payload];
  case actionTypes.ADD_PRODUCT_SUCCESS:
    return state.map((p) => {
      if (p.id === action.id) {
        return action.payload;
      }
      return p;
    });
  case actionTypes.EDIT_PRODUCT:
    return state.map((p) => {
      if (p.id === action.payload.id) {
        return action.payload;
      }
      return p;
    });
  case actionTypes.DELETE_PRODUCT:
    return state.filter(p => p.id !== action.payload.id);
  default:
    return state;
  }
};

export default products;
