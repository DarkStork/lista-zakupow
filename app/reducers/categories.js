import * as actionTypes from '../constants/actionTypes';
import { categories } from '../constants/categories';

const initState = [...categories];

const data = (state = initState, action) => {
  switch (action.type) {
  case actionTypes.GET_CATEGORIES_SUCCESS:
    return action.payload;
  default:
    return state;
  }
};

export default data;
