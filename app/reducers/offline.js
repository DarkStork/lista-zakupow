import moment from 'moment';
import * as actionTypes from '../constants/actionTypes';

const initState = {
  isOnline: true,
  listProductsToDelete: [],
  listProductsToAdd: [],
  listProductsToUpdate: [],
  avaProductsToDelete: [],
  avaProductsToAdd: [],
  avaProductsToUpdate: [],
};

const offline = (state = { ...initState }, action) => {
  switch (action.type) {
  case actionTypes.OFFLINE_CLEAR:
    return {
      isOnline: state.isOnline,
      listProductsToDelete: [],
      listProductsToAdd: [],
      listProductsToUpdate: [],
      avaProductsToDelete: [],
      avaProductsToAdd: [],
      avaProductsToUpdate: [],
    };
  case actionTypes.OFFLINE_SET_STATE:
    return { ...state, isOnline: action.payload };
  case actionTypes.OFFLINE_LIST_ADD: {
    const newState = { ...state };
    newState[action.list].push(action.payload);
    return newState;
  }
  case actionTypes.OFFLINE_LIST_ADD_UPDATE: {
    const newState = { ...state };
    const item = { ...action.payload, lastModified: moment().unix() };
    let updated = false;
    newState[action.updateList] = newState[action.updateList].map((p) => {
      if (p.id === item.id) {
        updated = true;
        return item;
      }
      return p;
    });
    if (!updated) newState[action.list].push(item);
    return newState;
  }

  default:
    return state;
  }
};

export default offline;
