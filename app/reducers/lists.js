import * as actionTypes from '../constants/actionTypes.js';

const initState = [];

const lists = (state = initState, action) => {
  switch (action.type) {
  case actionTypes.GET_LISTS_SUCCESS:
    return action.payload;
  case actionTypes.ADD_NEW_LIST:
    return [...state, action.payload];
  case actionTypes.ADD_LIST_PRODUCT:
  case actionTypes.ADD_LIST_PRODUCT_SUCCESS:
  case actionTypes.EDIT_LIST_PRODUCT:
  case actionTypes.DELETE_LIST_PRODUCT:
  case actionTypes.CHECK_LIST_PRODUCT:
  case actionTypes.UNCHECK_LIST_PRODUCT:
    return state.map((l) => {
      if (l.id !== action.payload.shoppingListId) {
        return l;
      }
      return list(l, action);
    });
  default:
    return state;
  }
};

const list = (state, action) => {
  switch (action.type) {
  case actionTypes.ADD_LIST_PRODUCT:
    return { ...state, products: [...state.products, action.payload] };
  case actionTypes.ADD_LIST_PRODUCT_SUCCESS:
    return {
      ...state,
      products: state.products.map((p) => {
        if (p.id === action.id) {
          return { ...action.payload, sync: true };
        }
        return p;
      }),
    };
  case actionTypes.EDIT_LIST_PRODUCT:
    return {
      ...state,
      products: state.products.map((p) => {
        if (p.id === action.payload.id) {
          return action.payload;
        }
        return p;
      }),
    };
  case actionTypes.DELETE_LIST_PRODUCT:
    return { ...state, products: state.products.filter(p => p.id !== action.payload.id) };
  case actionTypes.CHECK_LIST_PRODUCT:
    return {
      ...state,
      products: state.products.map((p) => {
        if (p.id === action.payload.id) {
          return { ...p, checked: true };
        }
        return p;
      }),
    };
  case actionTypes.UNCHECK_LIST_PRODUCT:
    return {
      ...state,
      products: state.products.map((p) => {
        if (p.id === action.payload.id) {
          return { ...p, checked: false };
        }
        return p;
      }),
    };
  default:
    return state;
  }
};

export default lists;
