import * as actionTypes from '../constants/actionTypes';

const initState = {
  authenticated: false,
  user: {},
};

const data = (state = initState, action) => {
  switch (action.type) {
  case actionTypes.LOGIN_SUCCESS:
    return { authenticated: true, user: action.payload };
  case actionTypes.LOGOUT_SUCCESS:
    return initState;
  default:
    return state;
  }
};

export default data;
