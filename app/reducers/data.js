import * as actionTypes from '../constants/actionTypes.js';

let initState = {
  data: [],
  loading: true,
};

const data = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.GET_SAMPLE_DATA:
      return { ...state, data: action.payload, loading: false };
    default:
      return state;
  }
};

export default data;
