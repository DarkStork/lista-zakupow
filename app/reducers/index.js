import { combineReducers } from 'redux';
import groups from './groups';
import lists from './lists';
import products from './products';
import nav from './nav';
import user from './user';
import categories from './categories';
import offline from './offline';

const reducer = combineReducers({
  groups,
  categories,
  lists,
  products,
  nav,
  user,
  offline,
});
export default reducer;
