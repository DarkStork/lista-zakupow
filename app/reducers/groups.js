import * as actionTypes from '../constants/actionTypes.js';

let initState = [];

const data = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.GET_GROUPS_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

export default data;
