import { NetInfo } from 'react-native';
import { store } from '../../store';
import * as actions from '../../actions/index';
import * as actionTypes from '../../constants/actionTypes';

class OfflineControler {
  constructor(interval) {
    this.isOnline = true;
    this.checkConnection = this.checkConnection.bind(this);
    this.interval = setInterval(this.checkConnection, interval);
  }

  checkConnection() {
    NetInfo.isConnected.fetch().then((isConnected) => {
      if (isConnected) {
        if (!this.isOnline) {
          this.updateOffline();
        }
        this.isOnline = true;
        this.updateOnline();
      } else {
        if (this.isOnline) {
          store.dispatch({
            type: actionTypes.OFFLINE_SET_STATE,
            payload: false,
          });
        }
        this.isOnline = false;
      }
    });
  }

  updateOffline() {
    const { offline } = store.getState();
    store.dispatch({
      type: actionTypes.OFFLINE_SET_STATE,
      payload: true,
    });
    const {
      listProductsToAdd,
      listProductsToDelete,
      listProductsToUpdate,
      avaProductsToAdd,
      avaProductsToDelete,
      avaProductsToUpdate,
    } = offline;

    // Getting fresh data
    store.dispatch(actions.getInitData()).then(() => {
      // Add products to lists
      listProductsToAdd.forEach((product) => {
        store.dispatch(actions.addListProduct(product));
      });

      // Add ava. products
      avaProductsToAdd.forEach((product) => {
        store.dispatch(actions.addAvaProduct(product));
      });

      // Delete products from lists
      listProductsToDelete.forEach((product) => {
        store.dispatch(actions.deleteListProduct(product));
      });

      // Delete ava. products
      avaProductsToDelete.forEach((product) => {
        store.dispatch(actions.deleteAvaProduct(product));
      });

      // Update products from lists
      setTimeout(() => {
        const { lists, products } = store.getState();
        listProductsToUpdate.forEach((product) => {
          const productList = lists.find(l => l.id === product.shoppingListId);
          if (productList) {
            const currentProduct = productList.products.find(p => p.id === product.id);
            if (currentProduct && currentProduct.lastModified < product.lastModified * 1000) {
              store.dispatch(actions.editListProduct(product));
            }
          }
        });
        avaProductsToUpdate.forEach((product) => {
          const currentProduct = products.find(p => p.id === product.id);
          if (currentProduct && currentProduct.lastModified < product.lastModified * 1000) {
            store.dispatch(actions.editAvaProduct(product));
          }
        });
      }, 1000);

      // Clear sync store
      store.dispatch({ type: actionTypes.OFFLINE_CLEAR });
    });
  }

  updateOnline() {}
}

export default OfflineControler;
