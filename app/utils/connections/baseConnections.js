import axios from 'axios';

import store from '../../store.js';

class BaseConnections {
  constructor(connections, authentication) {
    this.base_url = connections.core;
    this.authentication = authentication;
    this.auth_token = '';
    this.conn = axios.create({
      timeout: 300 * 1000,
    });
  }

  setAuthToken(token) {
    this.authentication = token;
    this.conn.defaults.headers.common.Authorization = `AUTH-TOKEN token="${token}"`;
  }

  getJSON(url, type = 'regular') {
    return new Promise((resolve, reject) => {
      this.conn
        .get(url, { type })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  postJSON(url, payload, type = 'regular') {
    return new Promise((resolve, reject) => {
      this.conn
        .post(url, payload, { type })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  putJSON(url, payload, type = 'regular') {
    if (this.demoMode) return emptyPromise(payload);
    return new Promise((resolve, reject) => {
      this.conn
        .put(url, payload, { type })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  deleteJSON(url, payload, type = 'regular') {
    return new Promise((resolve, reject) => {
      this.conn
        .delete(url, payload, { type })
        .then(response => {
          resolve(response);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
  all(arr) {
    return axios.all(arr);
  }
}

export default BaseConnections;
