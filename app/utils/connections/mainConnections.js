import BaseConnections from './baseConnections.js';

class Connections extends BaseConnections {
  constructor(connections, authentication) {
    super(connections, authentication);
  }

  login(loginData, requestType) {
    return super.postJSON(`${this.base_url}/login`, loginData, requestType);
  }

  getGroups(requestType) {
    return super.getJSON(`${this.base_url}/groups`, requestType);
  }
  getGroupLists(groupId, requestType) {
    return super.getJSON(`${this.base_url}/groups/${groupId}/shopping-lists`, requestType);
  }

  getGroupCategories(groupId, requestType) {
    return super.getJSON(`${this.base_url}/groups/${groupId}/categories`, requestType);
  }

  getGroupAvaProducts(groupId, requestType) {
    return super.getJSON(`${this.base_url}/groups/${groupId}/available-products`, requestType);
  }

  getListContent(listId, requestType) {
    return super.getJSON(`${this.base_url}/shopping-lists/${listId}/products`, requestType);
  }

  addListProduct(product, requestType) {
    return super.postJSON(
      `${this.base_url}/shopping-lists/${product.shoppingListId}/products`,
      product,
      requestType
    );
  }

  addAvaProduct(product, requestType) {
    return super.postJSON(
      `${this.base_url}/groups/${product.groupId}/available-products`,
      product,
      requestType
    );
  }

  addNewList(list, requestType) {
    return super.postJSON(
      `${this.base_url}/groups/${list.groupId}/shopping-lists`,
      list,
      requestType
    );
  }

  editListProduct(product, requestType) {
    return super.putJSON(
      `${this.base_url}/shopping-list-products/${product.id}`,
      product,
      requestType
    );
  }

  editAvaProduct(product, requestType) {
    return super.putJSON(`${this.base_url}/available-products/${product.id}`, product, requestType);
  }
  deleteListProduct(product, requestType) {
    return super.deleteJSON(`${this.base_url}/shopping-list-products/${product.id}`, requestType);
  }
  deleteAvaProduct(product, requestType) {
    return super.deleteJSON(`${this.base_url}/available-products/${product.id}`, requestType);
  }
}

export default Connections;
