export const categories = [
  {
    id: 1,
    group_id: 1,
    color_key: '#B00B55',
    name: 'Mięso',
    is_deleted: false,
  },
  {
    id: 2,
    group_id: 1,
    color_key: '#BADA55',
    name: 'Ważywa',
    is_deleted: false,
  },
  {
    id: 3,
    group_id: 1,
    color_key: '#C0FFEE',
    name: 'Nabiał',
    is_deleted: false,
  },
  {
    id: 4,
    group_id: 1,
    color_key: '#C55',
    name: 'Owoce',
    is_deleted: false,
  },
];
