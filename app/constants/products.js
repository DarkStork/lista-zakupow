export const productsList1 = [
  {
    id: 1, group_id: 1, category_id: 4, name: 'Jabłka', unit: 'kg', quantity: 3,
  },
  {
    id: 2, group_id: 1, category_id: 2, name: 'Kalafior', unit: 'szt.', quantity: 1,
  },
  {
    id: 3, group_id: 1, category_id: 1, name: 'Szynka', unit: 'kg', quantity: 1,
  },
  {
    id: 4, group_id: 1, category_id: 3, name: 'Jajka', unit: 'szt.', quantity: 10,
  },
  {
    id: 5, group_id: 1, category_id: 1, name: 'Maślanka', unit: 'l', quantity: 1.5,
  },
  {
    id: 6, group_id: 1, category_id: 4, name: 'Sok pomarańczowy', unit: 'l', quantity: 3,
  },
  {
    id: 7, group_id: 1, category_id: 1, name: 'Pierś z kurczaka', unit: 'kg', quantity: 2,
  },
  {
    id: 8, group_id: 1, category_id: 2, name: 'Ryż', unit: 'kg', quantity: 1,
  },
];
