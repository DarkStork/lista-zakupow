import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  Image,
  TouchableOpacity,
} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
      error: null,
    };
    this.login = this.login.bind(this);
  }
  componentDidMount() {
    if (this.props.user.authenticated) {
      this.props.navigation.navigate('Listy');
      if (this.props.offline.isOnline) {
        this.props.getInitData();
      }
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.user.authenticated && nextProps.offline.isOnline) {
      this.props.navigation.navigate('Listy');
    }
  }
  login() {
    const { login, password } = this.state;
    this.props
      .login({ login, password })
      .then(() => {
        if (this.state.error) this.setState({ error: null });
      })
      .catch(() => {
        this.setState({ error: 'Nieprawidłowe dane logowania', password: '' });
      });
  }
  render() {
    const isOffline = !this.props.offline.isOnline;
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <Image
          source={require('../../../content/loginBackground1.jpg')}
          style={styles.backgroundImage}
        />
        <View style={styles.formContainer}>
          <View style={styles.logoContainer}>
            <MaterialCommunityIcons name="cart-plus" size={64} color="#FBFBFF" />
            <Text style={styles.logotype}>Lista zakupów</Text>
          </View>
          <Text style={styles.inputLabel}>Login</Text>
          <TextInput
            style={styles.textInput}
            underlineColorAndroid="transparent"
            selectionColor="#406cf6"
            onChangeText={text => this.setState({ login: text })}
            value={this.state.login}
          />
          <Text style={styles.inputLabel}>Hasło</Text>
          <TextInput
            style={styles.textInput}
            underlineColorAndroid="transparent"
            selectionColor="#406cf6"
            secureTextEntry
            onChangeText={text => this.setState({ password: text })}
            value={this.state.password}
          />
          {isOffline ? (
            <View style={[styles.button, styles.buttonDisabled]}>
              <Text style={styles.buttonText}>Brak połączenia</Text>
            </View>
          ) : (
            <TouchableOpacity onPress={this.login}>
              <View style={styles.button}>
                <Text style={styles.buttonText}>Zaloguj</Text>
              </View>
            </TouchableOpacity>
          )}

          {this.state.error ? <Text style={styles.errorMsg}>{this.state.error}</Text> : null}
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    backgroundColor: '#ccc',
    flex: 1,
    resizeMode: 'cover',
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
  inputLabel: {
    color: '#FBFBFF',
    fontSize: 14,
    fontWeight: '400',
    marginVertical: 10,
    textAlign: 'center',
  },
  formContainer: {
    justifyContent: 'center',
    alignItems: 'stretch',
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#406CF6',
  },
  button: {
    height: 40,
    marginHorizontal: 50,
    backgroundColor: '#2854E2',
    borderRadius: 3,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 1,
  },
  buttonDisabled: {
    backgroundColor: '#AAA',
  },
  buttonText: {
    color: '#FBFBFF',
    fontWeight: '600',
  },
  textInput: {
    backgroundColor: '#FBFBFF',
    height: 40,
    borderColor: '#406cf6',
    borderWidth: 1,
    borderRadius: 3,
    marginHorizontal: 50,
    paddingHorizontal: 10,
  },
  logoSection: {
    height: 200,
    backgroundColor: 'red',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
  },
  logotype: {
    color: '#FBFBFF',
    fontSize: 18,
    fontWeight: '600',
    marginTop: 10,
    textAlign: 'center',
  },
  errorMsg: {
    color: '#FBFBFF',
    fontWeight: '700',
    textAlign: 'center',
    marginTop: 20,
  },
});
export default Products;
