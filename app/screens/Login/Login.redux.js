import { connect } from 'react-redux';
import Login from './Login';
import { login, getInitData } from '../../actions/index';

const mapStateToProps = state => ({
  user: state.user,
  offline: state.offline,
});

const mapDispatchToProps = {
  login,
  getInitData,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
