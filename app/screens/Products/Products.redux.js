import { connect } from 'react-redux';
import Products from './Products';
import {
  addAvaProduct,
  editAvaProduct,
  deleteAvaProduct,
  addListProduct,
} from '../../actions/index';

const mapStateToProps = state => ({
  lists: state.lists,
  categories: state.categories,
  products: state.products,
});

const mapDispatchToProps = {
  addAvaProduct,
  editAvaProduct,
  deleteAvaProduct,
  addListProduct,
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);
