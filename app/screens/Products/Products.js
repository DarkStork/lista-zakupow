import React, { Component } from 'react';
import { StyleSheet, FlatList, View, Text, TouchableOpacity } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import Header from '../../components/Header/Header.redux';
import SelectModal from '../../components/SelectModal/SelectModal';
import ProductItem from '../../components/ProductItem/ProductItem.redux';
import { uuid4 } from '../../utils/base';

class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      itemToAdd: null,
    };
    this.toggleModal = this.toggleModal.bind(this);
    this.addToList = this.addToList.bind(this);
    this.pickList = this.pickList.bind(this);
  }
  toggleModal() {
    this.setState({ showModal: !this.state.showModal });
  }
  addToList(item) {
    this.setState({ itemToAdd: item });
    this.toggleModal();
  }
  pickList(listId) {
    this.props.addListProduct({
      ...this.state.itemToAdd,
      id: uuid4(),
      shoppingListId: listId,
    });
    this.toggleModal();
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <SelectModal
          onClick={this.pickList}
          items={this.props.lists}
          showModal={this.state.showModal}
        />

        <Header>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.openDrawer();
            }}
          >
            <MaterialIcons name="menu" size={32} color="#FBFBFF" />
          </TouchableOpacity>
          <Text style={styles.title}>Dostępne produkty</Text>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('itemForm', { isProduct: true });
            }}
          >
            <MaterialIcons name="add-circle-outline" size={32} color="#FBFBFF" />
          </TouchableOpacity>
        </Header>

        <FlatList
          data={this.props.products.map(product => ({ ...product, key: product.id.toString() }))}
          style={styles.listContainer}
          contentContainerStyle={{ paddingBottom: 30 }}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => (
            <ProductItem
              item={item}
              key={item.id}
              addToList={this.addToList}
              delete={this.props.deleteAvaProduct}
              navigation={this.props.navigation}
            />
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8fd',
  },
  listContainer: {
    flex: 1,
    height: 300,
    backgroundColor: '#f8f8fd',
  },
  itemTitleCrossed: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '400',
    flex: 0.7,
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  itemTitle: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '400',
    flex: 0.7,
  },
  itemQuantity: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '300',
    flex: 0.2,
    flexDirection: 'row',
  },
  itemQuantityCrossed: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '300',
    flex: 0.2,
    flexDirection: 'row',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  listPicker: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  title: {
    color: '#FBFBFF',
    fontSize: 18,
    fontWeight: '600',
    textAlign: 'center',
    flex: 1,
  },
});

export default Products;
