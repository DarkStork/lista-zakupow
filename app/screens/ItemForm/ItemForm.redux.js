import { connect } from 'react-redux';
import ItemForm from './ItemForm';
import {
  addListProduct,
  editListProduct,
  addAvaProduct,
  editAvaProduct,
} from '../../actions/index';

const mapStateToProps = state => ({
  lists: state.lists,
  groups: state.groups,
  categories: state.categories,
});

const mapDispatchToProps = {
  addListProduct,
  editListProduct,
  addAvaProduct,
  editAvaProduct,
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemForm);
