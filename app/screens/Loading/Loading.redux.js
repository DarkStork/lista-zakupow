import { connect } from 'react-redux';
import Loading from './Loading';
import { getInitData } from '../../actions/index';

const mapStateToProps = state => ({
  offline: state.offline,
});

const mapDispatchToProps = {
  getInitData,
};

export default connect(mapStateToProps, mapDispatchToProps)(Loading);
