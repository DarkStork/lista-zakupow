import { connect } from 'react-redux';
import Lists from './Lists';
import {
  addListProduct,
  deleteListProduct,
  checkListProduct,
  uncheckListProduct,
  buySelectedProducts,
} from '../../actions/index';

const mapStateToProps = state => ({
  lists: state.lists,
  groups: state.groups,
  offline: state.offline,
});

const mapDispatchToProps = {
  addListProduct,
  deleteListProduct,
  checkListProduct,
  uncheckListProduct,
  buySelectedProducts,
};

export default connect(mapStateToProps, mapDispatchToProps)(Lists);
