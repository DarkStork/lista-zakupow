import React, { Component } from 'react';
import { StyleSheet, FlatList, View, Text, StatusBar, TouchableOpacity } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { LinearGradient } from 'expo';
import ActionButton from 'react-native-action-button';
import ListItem from '../../components/ListItem/ListItem.redux';
import Header from '../../components/Header/Header.redux';
import Select from '../../components/Select/Select';

class Lists extends Component {
  constructor(props) {
    super(props);
    const selectedList = props.lists.length > 0 ? props.lists[0].id : null;
    this.state = {
      selectedList,
    };
    this.getActionIcon = this.getActionIcon.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (!this.state.selectedList && nextProps.lists.length !== this.props.lists.length) {
      this.setState({ selectedList: nextProps.lists[0].id });
    }
  }
  getActionIcon() {
    return <MaterialIcons name="add-shopping-cart" size={24} color="#FBFBFF" />;
  }
  render() {
    const selectedList = this.props.lists.find(list => list.id === this.state.selectedList) || {
      name: 'brak list',
      products: [],
    };
    const isOffline = !this.props.offline.isOnline;
    return (
      <View style={styles.container}>
        <Header>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.openDrawer();
            }}
          >
            <MaterialIcons name="menu" size={32} color="#FBFBFF" />
          </TouchableOpacity>
          <Text style={styles.title}>Lista zakupów</Text>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('itemForm', { listId: this.state.selectedList });
            }}
          >
            <MaterialIcons name="add-circle-outline" size={32} color="#FBFBFF" />
          </TouchableOpacity>
        </Header>
        <View style={styles.listSelectContainer}>
          <View style={{ flex: 1 }}>
            <Select
              label="Wybierz listę"
              data={this.props.lists.map(list => ({ ...list, value: list.name }))}
              containerStyle={styles.listPicker}
              value={selectedList.name}
              onChangeText={(text, index) => {
                this.setState({ selectedList: this.props.lists[index].id });
              }}
            />
          </View>

          <TouchableOpacity
            onPress={() => {
              if (!isOffline) this.props.navigation.navigate('listForm');
            }}
          >
            <MaterialIcons
              name="add"
              size={32}
              color={isOffline ? '#888' : '#406CF6'}
              style={styles.addListIcon}
            />
          </TouchableOpacity>
        </View>

        <FlatList
          data={selectedList.products.map(list => ({ ...list, key: list.name }))}
          style={styles.listContainer}
          contentContainerStyle={{ paddingBottom: 30, backgroundColor: '#f8f8fd' }}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item }) => (
            <ListItem
              item={item}
              delete={this.props.deleteListProduct}
              check={this.props.checkListProduct}
              uncheck={this.props.uncheckListProduct}
              navigation={this.props.navigation}
            />
          )}
        />
        <LinearGradient
          colors={['rgba(238,238,238,0)', 'rgba(238,238,238,1)']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            height: 50,
          }}
        />
        {selectedList.products.filter(p => p.checked).length > 0 ? (
          <ActionButton
            buttonColor="#406CF6"
            offsetX={15}
            offsetY={15}
            renderIcon={this.getActionIcon}
            fixNativeFeedbackRadius
            onPress={() => {
              this.props.buySelectedProducts(this.state.selectedList);
            }}
          />
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8fd',
  },
  listContainer: {
    flex: 1,
    backgroundColor: '#f8f8fd',
  },
  itemTitleCrossed: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '400',
    flex: 0.7,
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  itemTitle: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '400',
    flex: 0.7,
  },
  itemQuantity: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '300',
    flex: 0.2,
    flexDirection: 'row',
  },
  itemQuantityCrossed: {
    color: '#0B4F6C',
    fontSize: 16,
    fontWeight: '300',
    flex: 0.2,
    flexDirection: 'row',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  listPicker: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  title: {
    color: '#FBFBFF',
    fontSize: 18,
    fontWeight: '600',
  },
  header: {
    height: 50 + StatusBar.currentHeight,
    paddingTop: StatusBar.currentHeight,
    backgroundColor: '#406CF6',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  listSelectContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  addListIcon: {
    marginRight: 20,
    marginTop: 20,
  },
});

export default Lists;
