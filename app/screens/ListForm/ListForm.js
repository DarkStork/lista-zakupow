import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { MaterialIcons } from '@expo/vector-icons';
import { StyleSheet, View, Text, StatusBar, TouchableOpacity } from 'react-native';
import TextInput from '../../components/TextInput/TextInput';
import Select from '../../components/Select/Select';
import Header from '../../components/Header/Header.redux';
import { uuid4 } from '../../utils/base';

class ListForm extends Component {
  constructor(props) {
    super(props);

    this.state = this.getInitState();
    this.submitFunc = this.submitFunc.bind(this);
  }
  getInitState() {
    const { groups } = this.props;
    let group = { name: '' };
    if (this.props.groups.length > 0) group = this.props.groups[0];
    return {
      name: '',
      group,
      error: null,
    };
  }
  submitFunc() {
    const { group, name } = this.state;
    if (!name) {
      this.setState({ error: 'Proszę podać nazwę' });
      return;
    }
    if (this.state.error) {
      this.setState({ error: null });
    }
    this.props.addNewList({
      groupId: group.id,
      name,
    });

    this.props.navigation.navigate('Listy');
  }
  render() {
    return (
      <View style={styles.container}>
        <Header>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.goBack();
            }}
          >
            <MaterialIcons name="arrow-back" size={32} color="#FBFBFF" />
          </TouchableOpacity>

          <Text style={styles.title}>Dodaj nową listę</Text>
        </Header>
        <View style={styles.formContainer}>
          <Text style={styles.label}>Nazwa</Text>
          <TextInput
            placeholder="Nazwa"
            onChangeText={name => this.setState({ name })}
            value={this.state.name}
          />

          <Select
            label="Grupa"
            data={this.props.groups.map(g => ({ ...g, value: g.name }))}
            value={this.state.group.name}
            onChangeText={(text, index) => {
              this.setState({ group: this.props.groups[index] });
            }}
          />

          <TouchableOpacity onPress={this.submitFunc}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>Dodaj</Text>
            </View>
          </TouchableOpacity>

          {this.state.error ? <Text style={styles.errorMsg}>{this.state.error}</Text> : null}
        </View>
      </View>
    );
  }
}

ListForm.propTypes = {
  navigation: PropTypes.shape({
    state: PropTypes.shape({
      params: PropTypes.shape({
        listId: PropTypes.number,
      }),
    }),
    navigate: PropTypes.func,
  }).isRequired,
  addListProduct: PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listPicker: {
    borderColor: 'red',
  },
  container2: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    padding: 20,
  },
  label: {
    color: '#333',
    fontSize: 14,
    fontWeight: '400',
    marginVertical: 10,
    textAlign: 'center',
  },
  title: {
    color: '#FBFBFF',
    fontSize: 18,
    fontWeight: '600',
    flex: 1,
    textAlign: 'center',
    marginRight: 40,
  },
  header: {
    height: 50,
    marginTop: StatusBar.currentHeight,
    backgroundColor: '#406CF6',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  quantity: {
    flex: 0.7,
  },
  uint: {
    flex: 0.3,
  },
  button: {
    height: 40,
    backgroundColor: '#406CF6',
    borderRadius: 3,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#FBFBFF',
    fontWeight: '600',
  },
  errorMsg: {
    color: 'red',
    textAlign: 'center',
    marginTop: 20,
  },
});

export default ListForm;
