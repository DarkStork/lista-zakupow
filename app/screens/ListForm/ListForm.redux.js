import { connect } from 'react-redux';
import ListForm from './ListForm';
import { addNewList } from '../../actions/index';

const mapStateToProps = state => ({
  lists: state.lists,
  groups: state.groups,
  categories: state.categories,
});

const mapDispatchToProps = {
  addNewList,
};

export default connect(mapStateToProps, mapDispatchToProps)(ListForm);
