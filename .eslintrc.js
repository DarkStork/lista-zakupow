module.exports = {
  parser: 'babel-eslint',
  rules: {
    strict: 0
  },
  env: {
    browser: true,
    es6: true,
    jquery: true
  },
  extends: 'airbnb',
  parserOptions: {
    sourceType: 'module'
  },
  plugins: ['react'],
  rules: {
    'comma-dangle': ['warn', 'always-multiline'],
    indent: ['warn', 2],
    'linebreak-style': ['warn', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'always'],
    /* Advanced Rules*/
    'no-unused-expressions': 'warn',
    'no-useless-concat': 'warn',
    'block-scoped-var': 'error',
    'consistent-return': 'error',
    'no-use-before-define': 0,
    'react/jsx-filename-extension': 0,
    'import/prefer-default-export': 0,
    'no-bitwise': 0,
    'class-methods-use-this': 0
  }
};
